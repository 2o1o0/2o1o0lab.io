---
title: Homelab
featured_image: 'images/homelab.jpg'
omit_header_text: true
description: Home stuff
type: page
menu: main
---

# Homelab

## Network

Running a [IPv4/IPv6 stack](https://kubernetes.io/docs/concepts/services-networking/dual-stack/) protected by [Cloudflare](https://www.cloudflare.com/) as my ISP only offers CGNAT on IPv4.

More than 20 wired/wireless clients including IoT.

Core network was originally done with ISP router and a Cisco 3560 PoE but was migrated to a Ubiquity switch and firewall for power consumption and IPv4/IPv6 proper configuration.

Cisco switch configuration was pushed by [AWX](https://www.ansible.com/awx/) but is now decommissioned. 
Ubiquity APIs allow for more [indepth monitoring](https://grafana.com/grafana/dashboards/?search=unifi-poller).

## Kubernetes cluster

### Hardware

* A Synology NAS for the storage
* A collection of 3 low budget / refurbished computers, for the compute
* A couple of Raspberry Pi for critical services like DHCP/DNS
* A NVidia 4070 on a gaming computer for AI tasks

### Software

* Running [Talos](https://www.talos.dev/) as OS since 2021
* [Synology CSI](https://github.com/SynologyOpenSource/synology-csi) for storage backend
* [ArgoCD](https://argo-cd.readthedocs.io/en/stable/) for the helm charts deployment, currently managing 40 apps
* [Keycloak](https://www.keycloak.org/) for users management and Yubikey for [FIDO2](https://www.microsoft.com/en-us/security/business/security-101/what-is-fido2)
* [Ollama](https://ollama.com/) and [Openwebui](https://docs.openwebui.com/) for AI tasks
* [Prometheus](https://prometheus.io/)/[Loki](https://grafana.com/docs/loki/latest/)/[Grafana](https://grafana.com/docs/grafana/latest/) stack for observability
* Postgresql/MariaDB together with [db-operator](https://github.com/db-operator/db-operator) for centralised databases
* [Pi-hole](https://pi-hole.net/) for DNS server and "house-wide" ads filtering
* a ***LOT*** of softwares from the [opensource community](https://github.com/awesome-selfhosted/awesome-selfhosted)
* [Renovate](https://www.mend.io/renovate/) to keep it up-to-date