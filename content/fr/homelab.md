---
title: Homelab
featured_image: 'images/homelab.jpg'
omit_header_text: true
description: Home stuff
type: page
menu: main
---
# Homelab

## Réseau


J'utilise une [stack IPv4/IPv6](https://kubernetes.io/docs/fr/concepts/services-networking/dual-stack/) protégé par [Cloudflare](https://www.cloudflare.com/fr) puisque mon fournisseur ne propose que du CGNAT sur IPv4.

J'ai plus de 20 clients câblés/sans-fil, y compris des objets connectés (IoT).

Le réseau de base a été mis en oeuvre avec un routeur de mon FAI et un Cisco 3560 PoE mais j'ai migré vers un commutateur et un pare-feu Ubiquity afin de réduire la consommation d'énergie et une meilleure configuration IPv4/IPv6.

La configuration du switch Cisco était propagée par [AWX](https://www.ansible.com/fr/awx/) je l'ai déinstallé.
Les APIs d'Ubiquity permettent une supervision plus approfondie avec [Grafana](https://grafana.com/grafana/dashboards/?search=unifi-poller).

## Cluster Kubernetes

### Matériel


* Un NAS Synologie pour le stockage
* Un ensemble de 3 ordinateurs bas coût / reconditionnés, pour le "compute"
* Deux Raspberry Pi pour les services critiques comme DHCP/DNS
* Une carte graphique NVidia 4070 sur un PC de jeux pour les tâches d'IA

### Logiciels


* Utilise [Talos](https://www.talos.dev/) en tant qu'OS depuis 2021
* Synology CSI pour le backend de stockage
* [ArgoCD](https://argo-cd.readthedocs.io/fr/stable/) pour la gestion des charts helm, gère actuellement 40 applications
* [Keycloak](https://www.keycloak.org/fr) pour gérer les utilisateurs et Yubikey pour [FIDO2](https://docs.microsoft.com/fr-fr/security/business/fido2)
* [Ollama](https://ollama.com/) et [Openwebui](https://docs.openwebui.com/fr/) pour les tâches d'IA
* La stack observabilité [Prometheus](https://prometheus.io/)/[Loki](https://grafana.com/docs/loki/latest/fr/)/[Grafana](https://grafana.com/grafana/dashboards/?search=unifi-poller)
* Postgresql/MariaDB avec le [db-operator](https://github.com/db-operator/db-operator) pour gérer les bases de données
* [Pi-hole](https://pi-hole.net/) afin d'avoir un serveur DNS et de filtrer les publicités sur l'ensemble du réseau
* ***Beaucoup*** d'autres logiciels de la communautée [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
* [Renovate](https://www.mend.io/renovate/) afin garder le tout à jour